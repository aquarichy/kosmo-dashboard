#!/bin/env bash

# Source/import this into other scripts

function kdb_site_up {
    URL="${1}";
    EXPECTED_RESPONSE="${2:-200}";  # expected response code; leave blank to assume 200
    EXPECTED_OUTPUT="${3}";  # expected response body as a string; leave blank to skip testing

    mkdir -p /tmp/kosmodashboard-tmp
    DEST="$(mktemp -p /tmp/kosmodashboard-tmp)";

    # curl:
    # --connect-timeout in seconds
    # -f  quick fail
    # -s  silent mode (e.g. --no-progress-meter
    # -L  location, follow redirects
    # -o  HTML output
    # -w  write X to stderr, http_code: HTTP Response Codee
    if RESPONSE="$(curl --connect-timeout 10 -fsL -o "${DEST}" -w "%{http_code}" "${URL}")" && [ "${RESPONSE}" == "${EXPECTED_RESPONSE}" ]; then
        if [ -n "${EXPECTED_OUTPUT}" ]; then
            echo "EXPECTED_OUTPUT supplied.  Comparing against actual output:";
            diff "${DEST}" <(echo -n "${EXPECTED_OUTPUT}");
            return $?
        else
            return 0;
        fi
    elif [ "${RESPONSE}" == "${EXPECTED_RESPONSE}" ]; then
        # the above command failed (non-zero exit value), but we may be testing an error code, so success!
        echo "Page load failed, giving expected response code ${EXPECTED_RESPONSE}";
        return 0;
    else
        echo "ERROR: HTTP response code: ${RESPONSE}.  Expected ${EXPECTED_RESPONSE}";
        return 1;
    fi
}

function kdb_dir_check_no_files {
    DIR=$1;
    TYPE="${2:-f}"; # e.g. "f", "d", "f,d";  defaults to f

    test -d "${DIR}/" || return 1;

    # find the number of regular files within DIR, excluding directories and dot files
    NUM_FILES="$(find "${DIR}". -mindepth 1 -maxdepth 1 -type "${TYPE}" -not -name ".*" | wc -l)";

    echo "${NUM_FILES} files to sort";
    test "${NUM_FILES}" -eq 0 && return 0 || return 2;
}

function kdb_ping_test {
    HOST=$1

    printf "ping %-30s " "${HOST}...";
    if ping -q -c 1 "${HOST}" >/dev/null 2>&1 ; then
        echo "✔️  Found";
        return 0;
    else
        echo "❌ Unavailable";
        return 1
    fi
}

function kdb_net_ip_test {
    EXPECTED_IP="${1}";
    HOST="${2}";
    W="${W:=30}"; # W can be set externally so that multiple calls will align correctly

    printf "ip %-${W}s " "${HOST}...";

    RESOLVED_IP="$(getent hosts "${HOST}" | cut -d ' ' -f 1)";
    if [ "${RESOLVED_IP}" == "${EXPECTED_IP}" ]; then
        echo "✔️  Matches";
        return 0;
    else
        echo "❌ Resolved to '${RESOLVED_IP}' instead";
        return 1;
    fi
}

function kdb_ssh_test {
    # Assumes pubkey authentication

    USER=$1
    HOST=$2
    PORT=$3

    printf "ssh %-30s  " "${HOST}...";
    if OUTPUT="$(ssh -o PasswordAuthentication=no -o ConnectTimeout=5 -p "${PORT}" "${USER}"@"${HOST}" 'echo -n "success"' </dev/null)" && [ "${OUTPUT}" == "success" ]; then
        echo "✔️  Success"
        return 0;
    else
        echo "❌ Failure";
        return 1;
    fi
}

function _kdb_upgrade_date_apt_extract {
    # fold multi-line records into one, split by '{NL}_'
    sed  -r -e 's;$;{NL};g' |
        tr '\n' '_'  |
        sed -r -e 's;\{NL\}_\{NL\}_;\n;g' |
        sed -r -e 's;^\{NL\}_;;g' |
        # find an upgrade with an 'End-Date' and
        grep -P "\}_Commandline: apt upgrade\{.*\}_End-Date: " |
        tail -n 1 |
        sed -r -e 's;.*\}_End-Date: ([^\{]*).*;\1;g'
}

function kdb_upgrade_date_apt_remote {
    USER_HOST=$1   # e.g. "mercury.local",   "zeus@mercury.local"
    EXTRA_ARG=$2   # e.g. "-p 4422"

    _kdb_upgrade_date_apt_extract < <(ssh -o PasswordAuthentication=no -o ConnectTimeout=5 "${USER_HOST}" "${EXTRA_ARG}" 'cat /var/log/apt/history.log' </dev/null);
}

function kdb_upgrade_date_apt {
    _kdb_upgrade_date_apt_extract <"/var/log/apt/history.log";
}

function kdb_upgrade_date_dnf {
    # `dnf history list` output:
    #     ID  | Command line | Date and time    | Action(s) | Altered
    #     -----------------------------------------------------------
    #     471 |              | 2023-11-16 16:43 | Upgrade   |   24
    #     470 | update       | 2023-11-14 01:11 | Upgrade   |   15

    # LANG=C to get 'Y-m-d' and avoid localization
    LANG=C dnf history list |
        grep -P "^[^\|]+\|[^\|]+\|[^\|]+\|[^\|]+\b(U|Upgrade)\b"  |
        head -n 1 |
        cut -d '|' -f 3 |
        sed -r -e 's;^ ;;g' | sed -r -e 's; $;;g'
}

function kdb_activity_date_compare {
    # Provide within how many days ago an activity should have been completed (e.g. backup, upgrade, etc.)
    # as well as the DT it was completed (parsable by `date`)
    N_DAYS="$1";
    ACTIVITY_DT="$2";

    if [ -z "${ACTIVITY_DT}" ]; then
        printf "❌ Provided activity date was empty.  Failing to compare.\n";
        return 2;
    fi

    THRESHOLD_STR="${N_DAYS} days ago";
    THRESHOLD_DT="$(date '+%Y-%m-%d %H:%M' -d "${THRESHOLD_STR}")";
    THRESHOLD_SEC="$(date '+%s' -d "${THRESHOLD_DT}")";

    TODAY_DT="$(date -Is)";
    TODAY_SEC="$(date '+%s' -d "${TODAY_DT}")";

    ACTIVITY_SEC="$(date '+%s' -d "${ACTIVITY_DT}")"

    (( DAY_DIFF=( TODAY_SEC - ACTIVITY_SEC ) / 60 / 60 / 24 ))

    if [ "${ACTIVITY_SEC}" -lt "${THRESHOLD_SEC}" ]; then
        printf "❌ Last occurrence on %s (%d days ago) older than threshold %s (%s)\n" "${ACTIVITY_DT}" "${DAY_DIFF}" "${THRESHOLD_DT}" "${THRESHOLD_STR}";
        return 1;
    else
        printf "✔️  Last occurrence within threshold\n";
        return 0;
    fi
}

function _kdb_conf_get_value {
    KEY_RE=$1

    if [ -f ~/.config/kosmodashboard.conf ]; then
        grep "^${KEY_RE}=" < ~/.config/kosmodashboard.conf | cut -d '=' -f 2
    fi
}

function kdb_net_wait_online_or_fail {
    TIMEOUT=$1    # Number of tries (one per second) until we give up on being online and fail
    IP=$2

    if [ -z "${TIMEOUT}" ]; then
        if ! TIMEOUT="$(_kdb_conf_get_value "online-test-timeout")" || [ -z "${TIMEOUT}" ]; then
            TIMEOUT=5
        fi
    fi

    if [ -z "${IP}" ]; then
        if ! IP="$(_kdb_conf_get_value "online-test-ip")" || [ -z "${IP}" ]; then
            IP=8.8.8.8   # default to Google's DNS IP
        fi
    fi

    for _i in $(seq 1 "${TIMEOUT}"); do
        ping -q -c 1 "${IP}" >/dev/null 2>&1 && return 0;
        sleep 1;
    done

    printf "❌ Unable to reach internet (test IP %s, timeout %d)\n" "${IP}" "${TIMEOUT}" >&2;
    return 1;
}

export -f \
       _kdb_upgrade_date_apt_extract \
       _kdb_conf_get_value

export -f \
       kdb_site_up \
       kdb_dir_check_no_files \
       kdb_ping_test \
       kdb_net_ip_test \
       kdb_ssh_test \
       kdb_activity_date_compare \
       kdb_upgrade_date_dnf \
       kdb_upgrade_date_apt \
       kdb_upgrade_date_apt_remote \
       kdb_net_wait_online_or_fail
