#!/bin/env bash

# KosmoDashboard: runs report scripts in ./reports/

PREFIX="$(dirname "$(dirname "$(realpath "$0")")")";
LIB_DIR="${PREFIX}/lib";
REPORTS_DIR="${PREFIX}/share/kosmodashboard/reports/";

VERBOSE=1;

# import libkosmodashboard.sh that exports functions for reports to use
. "${LIB_DIR}/libkosmodashboard.sh";

function print_usage {
    cat <<EOF
$(basename "$0") [-bh] [-r REPORTS_DIR]  -- Run a dashboard of reports

Options:

  -n, --notify    Show a notification on report completion
  -b, --brief     Don't show output of individual reports, just pass/fail status.
  -h, --help      Print this help screen
  -r, --reports   Specify an alternative reports directory instead of the default
                  (${REPORTS_DIR})
EOF
}

function run_report {
    echo "" >&${OUTFD};
    printf "%3d/%d %-${W}s " "${i}" "${NUM_REPORTS}" "$(basename "${REPORT}")...";
    echo "" >&${OUTFD};

    # shellcheck disable=SC2261
    OUTPUT="$("${REPORT}" >&${OUTFD} 2>&${ERRFD})";
    EVAL=$?

    (
        test -n "${OUTPUT}" && echo "${OUTPUT}";
        if [ "${EVAL}" -eq 0 ]; then
            echo "✔️  Pass";
        else
            echo "❌ FAIL";
        fi
    ) | sed -r -e 's;^;       ;g';

    return $EVAL;
}

function find_reports {
    REPORTS_DIR=$1

    find "${REPORTS_DIR}" -type f -executable -name "*.sh" | sort
}

NOTIFY=0;
IS_R_DIR=0;

for ARG in "$@"; do
    if [ "${ARG}" == "--" ]; then
        break;
    elif [ "${IS_R_DIR}" -eq 1 ]; then
        REPORTS_DIR="${ARG}";
        IS_R_DIR=0;
    elif [ "${ARG:0:1}" == "-" ]; then
        if [ "${ARG}" == "-h" ] || [ "${ARG}" == "--help" ]; then
            print_usage;
            exit 0;
        elif [ "${ARG}" == "-b" ] || [ "${ARG}" == "--brief" ]; then
            VERBOSE=0;
        elif [ "${ARG}" == "-r" ] || [ "${ARG}" == "--reports" ]; then
            IS_R_DIR=1;
            continue;
        elif [ "${ARG}" == "-n" ] || [ "${ARG}" == "--notify" ]; then
            NOTIFY=1;
            continue;
        fi
    fi
done

if [ "${IS_R_DIR}" -eq 1 ]; then
    echo "ERROR: -r given but not followed with a valid directory argument";
    exit 2;
fi

# Control how much info/warning/err output we show from the reports we run
if [ "${VERBOSE}" -eq 0 ]; then
    exec {OUTFD}<>"/dev/null"
    exec {ERRFD}<>"/dev/null"
else
    OUTFD=1
    ERRFD=2
fi

if ! [ -d "${REPORTS_DIR}" ]; then
    echo "ERROR: reports directory '${REPORTS_DIR}' not found." >&2;
    exit 1;
fi

NUM_REPORTS="$(find "${REPORTS_DIR}" -type f -executable -name "*.sh" | wc -l)";

# Determine longest report name width, for columnal print alignment
W=40;
while read -r REPORT; do
    REPORT_NAME="$(basename "${REPORT}")...";
    if [ "${#REPORT_NAME}" -gt "${W}" ]; then
        (( W="${#REPORT_NAME}" ))
    fi
done < <(find_reports "${REPORTS_DIR}")

# Run reports in REPORTS_DIR
echo "$(date -Is): $(hostname): Running ${NUM_REPORTS} reports...";
i=1
NUM_PASS=0
NUM_FAIL=0
FAILED_REPORTS="";

echo "Checking access to Internet...";
if ! kdb_net_wait_online_or_fail 2>&1; then
    echo "WARNING: unable to reach Internet.  Some tests may fail.";
else
    echo "✔️  Online";
fi 2>&1 | sed -r -e 's;^;       ;g';

while read -u "${reports_fd}" -r REPORT; do # -u, in case a report reads from stdin itself.
    if run_report; then
        (( NUM_PASS++ ))
    else
        (( NUM_FAIL++ ))
        FAILED_REPORTS="${FAILED_REPORTS}\n  ❌ $(basename "${REPORT}")";
    fi
    (( i++ ));
done {reports_fd}< <(find_reports "${REPORTS_DIR}")

# Warn about any .sh reports that are not executable
if NUM_NONEXEC="$(find "${REPORTS_DIR}" -type f -not -executable -name "*.sh" | wc -l)" && [ "${NUM_NONEXEC}" -gt 0 ]; then
    printf "NOTE: %d non-executable report(s):\n    " "${NUM_NONEXEC}";

    find "${REPORTS_DIR}" -type f -not -executable -name "*.sh" |
        sort |
        while read -r FILE; do
            BASE="$(basename "${FILE}")";
            if [ "${BASE:0:3}" != "lib" ]; then
                echo -n " ${BASE},"
            fi
        done |
        sed -r -e 's;,$;;g' &&
        echo "";
fi


# if notification (-n) requested, show a simple count
if [ "${NOTIFY}" -eq 1 ]; then
    if [ "${NUM_FAIL}" -gt 0 ]; then
        notify-send "kosmodashboard" "❌ ${NUM_FAIL} issues.  ✔️ ${NUM_PASS} passed.${FAILED_REPORTS}";
    else
        notify-send "kosmodashboard" "✔️ all (${NUM_PASS}) passed.";
    fi
fi
