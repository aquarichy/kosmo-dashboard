# KosmoDashboard

This is going to be a system that reports on the status of various
local tasks on my computer.

It will start as a shell script, but will one day be a daemon.  Needs to be
modular.  Eventually it will have an AppIndicator with a colour-blind-friendly
visual indicator as to the current state (e.g. all pass, any failures?) that will give you a drop out
of reports.

There may also be an option to run reports periodically (e.g. every 5 minutes)
and emit pop-up notifications on any new failures, etc.

May add some severity, e.g. yellow warnings vs. red alerts.

## Usage

Run `kosmodashboard_report.sh`.

Example output in default verbose mode would look like the following.  In this
example, a user has created 8 reports available in
`$PREFIX/share/kosmodashboard/reports/`.  (Most correspond to convenience
functions provided in `lib/libkosmodashboard.sh`, making them simple to create.)
They have a publicly accessible web domain at `jupiter.fake` and a local host on
their LAN available at `mercury.home.arpa` and `mercury.local` for mDNS.

``` shell
2023-11-15T17:50:28-08:00: juniperhost: Running 8 reports...

   1/8 check_emails.sh...
       0 e-mails to sort
       ✔️ Pass

   2/8 dir_todo.sh...
       0 files to sort
       ✔️ Pass

   3/8 dir_downloads.sh...
       0 files to sort
       ✔️ Pass

   4/8 free_space.sh...
       MNT          MB_AVAIL PERC
       /               49967   51
       /boot             622   32
       ✔️ Pass

   5/8 ip_jupiter.fake.sh...
       ip jupiter.fake...                  ✔️ Matches
       ✔️ Pass

   6/8 ip_lan_mercury.sh...
       ip mercury.home.arpa                ❌ Resolved to '192.168.0.55' instead
       ip mercury.local                    ✔️ Matches
       ❌ FAIL

   7/8 ping_ssh_jupiter.fake.sh...
       ping ftp.jupiter.fake...            ✔️ Found
       ssh ftp.jupiter.fake...             ✔️ Success
       ✔️ Pass

   8/8 siteup_jupiter.fake.sh...
       ✔️ Pass
```

For a terser mode, use `-b` / `--brief` to achieve this output:

``` shell
2023-11-15T17:50:46-08:00: juniperhost: Running 8 reports...
   1/8 check_emails.sh...                  ✔️ Pass
   2/8 dir_todo.sh...                      ✔️ Pass
   3/8 dir_downloads.sh...                 ✔️ Pass
   4/8 free_space.sh...                    ✔️ Pass
   5/8 ip_jupiter.fake.sh...               ✔️ Pass
   6/8 ip_lan_mercury.sh...                ❌ FAIL
   7/8 ping_ssh_jupiter.fake.sh...         ✔️ Pass
   8/8 siteup_jupiter.fake.sh...           ✔️ Pass
```

By default, it uses reports stored in `$PREFIX/share/kosmodashboard/reports/`. However, to point it to an alternate reports/ directory, you can use `-r` / `--reports`.
E.g. during development and testing, you can use a local directory, so: `./bin/kosmodashboard_report.sh -r ./reports`

## Configuration

Some settings can be defined at `~/.config/kosmodashboard.conf` such as:

- online-test-ip: an IP address used to test whether we can reach the Internet
  in calls to `kdb_net_wait_online_or_fail`, one of which happens before reports are run.
  Default: 8.8.8.8  (Google's DNS)
- online-test-timeout: the number of tries (one per second) that should be made by `kdb_net_wait_online_or_fail`
  before giving up.  Default: 5

See `config/kosmodashboard.conf.example` for an example file.

## Installation

Run `make install`

By default, `PREFIX=$HOME/.local`.  To install elsewhere, you can do:

`make PREFIX=/usr/local install`

Installed files include:

- `bin/kosmodashboard_report.sh` - runs reports
- `lib/libkosmodashboard.sh` - library of exported functions for reports to use
- `share/kosmodashboard/reports/` - a directory with a collection of `.example` reports

## make check

`make check` runs some linting on shell scripts with `shellcheck`, including those in `bin/`, `lib/` and any user reports in `share/kosmodashboard/reports/`.

## Tooling

- `bash`
- `Make`
- `shellcheck` - bash linting for `make check`

## Reports

Reports are individual executables, like executable shell scripts or compiled
binaries.  A report name is the name of the executable file itself.  For verbose
mode (default), an effort should be made for reports to integrate into the larger output.

`kosmodashboard_report.sh` automatically indents the output from reports. and
filters it out for brief (`-b`) mode.

Reports should exit with 0 for success and 0< for failures.

`kosmodashboard_report.sh` sources a library of exported functions from `lib/libkosmodashboard.sh`.
that scripts can make use for common dashboard test functionality.  Look inside `lib/libkosmodashboard.sh` to see what is offered.
You can find a small collection of example reports in `share/kosmodashboard/reports/` that should be
customized and have the `.example` suffix removed to be used.


## Copyright

Richard Schwarting &lt;<aquarichy@gmail.com>&gt; © 2023

## License

GPLv3+.  See LICENSE file.
