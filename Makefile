PREFIX=${HOME}/.local
# SYSTEMD_PREFIX: ${HOME}/.config vs. /usr/lib
SYSTEMD_PREFIX=${HOME}/.config

check: .shellcheck-passed

.shellcheck-passed: bin/*.sh lib/*.sh share/kosmodashboard/reports/*.sh.example
	shellcheck -e SC1091 $^
	touch $@

install:
	mkdir -p ${PREFIX}/bin
	install bin/kosmodashboard_report.sh ${PREFIX}/bin/
	mkdir -p ${PREFIX}/lib
	cp lib/libkosmodashboard.sh ${PREFIX}/lib/
	mkdir -p ${PREFIX}/share/kosmodashboard/reports/
	cp -r share/kosmodashboard/reports/*.example ${PREFIX}/share/kosmodashboard/reports/
	mkdir -p ${SYSTEMD_PREFIX}/systemd/user/
	cp systemd/user/kosmodashboard.{timer,service} ${SYSTEMD_PREFIX}/systemd/user/
	sed -i -r -e "s;BINDIR;${PREFIX}/bin;g" ${SYSTEMD_PREFIX}/systemd/user/kosmodashboard.service
	systemctl --user daemon-reload
	@echo ""
	@echo "NOTE: Run: 'systemctl --user enable --now kosmodashboard.timer' for recurring reports";

uninstall:
	rm -f ${PREFIX}/bin/kosmodashboard_report.sh
	rm -f ${PREFIX}/lib/libkosmodashboard.sh
	rm -f ${PREFIX}/share/kosmodashboard/reports/*.example
	rm -f ${SYSTEMD_PREFIX}/systemd/user/kosmodashboard.{timer,service}
	@echo "WARNING: leaving user-defined files in ${PREFIX}/share/kosmodashboard/reports/ in place, explicitly use \`uninstall_user_reports\` target to remove them"

uninstall_user_reports:
	rm -f ${PREFIX}/share/kosmodashboard/reports/*
	rmdir ${PREFIX}/share/kosmodashboard/reports
	rmdir ${PREFIX}/share/kosmodashboard

build/doc/README.html:
	mkdir -p build/doc/
	printf "<!DOCTYPE html>\n<html>\n  <head>\n    <meta charset='utf-8' />\n  </head>\n  <body>\n" > $@
	markdown_py -x fenced_code README.md >> $@
	printf "  </body>\n</html>\n" >> $@

clean:
	find . -name "*~" -delete -or -name "#*#" -delete
	rm -f .shellcheck-passed
	rm -f build/doc/README.html
	test ! -d build/doc || rmdir -p build/doc
